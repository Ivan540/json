#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QDebug>
#include <QFile>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QFile logFile("logfile.txt");
    QTextStream logStream(&logFile);
    QJsonDocument jsonDoc;
    if (logFile.open(QFile::WriteOnly|QFile::Append)){
        qDebug()<<"\n Logfile opened";
    }
    else qDebug() <<"\n Error can't open logfile.txt";
    QFile configFile ("config.json");
    if (configFile.exists()){
        configFile.open(QFile::ReadOnly);
        logStream<<"config.json opened";
        jsonDoc = QJsonDocument().fromJson(configFile.readAll());
        ui->txtConfig->setText(jsonDoc.toJson());
        QJsonObject obj = jsonDoc.object();
        logStream<<"\n"<<"ip:port="<<obj.value("ip").toVariant().toString()<<":"<<obj.value("port").toVariant().toString();
        logStream<<"\n"<<"frequency="<<obj.value("frequency").toVariant().toString()<<"\n\n";
        logFile.flush();
        logFile.close();
        logFile.open(QFile::ReadOnly);
        ui->txtLog->setText(logFile.readAll());
    }
    else{
        qDebug()<<"File not opened!";
        logStream<<"File not opened!";
    }

    configFile.close();
    logFile.close();
}

MainWindow::~MainWindow()
{
    delete ui;
}
